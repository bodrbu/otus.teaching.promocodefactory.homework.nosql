﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Helpers
{
    public static class MongoCollectionNameHelper
    {
        public static string GetCollectionName(Type entityType)
        {
            if (entityType == typeof(Customer))
            {
                return "Customers";
            }

            if (entityType == typeof(Preference))
            {
                return "Preferences";
            }

            if (entityType == typeof(PromoCode))
            {
                return "Promocodes";
            }

            throw new ArgumentException();
        }
    }
}
