﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo;
using Humanizer;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _dataContext;

        public MongoDbInitializer(IMongoDatabaseSettings dbSettings)
        {
            var client = new MongoClient(dbSettings.ConnectionString);
            _dataContext = client.GetDatabase(dbSettings.DatabaseName);
        }
        
        public void InitializeDb()
        {
            var preferenceCollectionName = nameof(Preference).Pluralize();
            _dataContext.DropCollection(preferenceCollectionName);
            var preferences =
                _dataContext.GetCollection<Preference>(preferenceCollectionName);
            preferences.InsertMany(FakeDataFactory.Preferences);

            var customersCollectionName = nameof(Customer).Pluralize();
            _dataContext.DropCollection(customersCollectionName);
            var customers = _dataContext.GetCollection<Customer>(customersCollectionName);
            customers.InsertMany(FakeDataFactory.Customers);
        }
    }
}