﻿using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Mongo
{
    public class MongoDatabaseSettings : IMongoDatabaseSettings
    {
        public string DatabaseName { get; set; }
        public string ConnectionString { get; set; }
    }

}
